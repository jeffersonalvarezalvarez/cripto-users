Rails.application.routes.draw do
  
  	devise_for :users, controllers: { registrations: 'users/registrations' }
  	
  	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  	authenticated :user do
    	root to: 'pages#form', as: :authenticated_root
  	end

  	devise_scope :user do
  		root to: "devise/sessions#new"
	end

	get "/upload" => 'pages#form', as: 'upload_page'
	get "/search" => 'pages#search', as: 'search_page'
  post "/get_parameters" => 'pages#parameters', as: 'parameters_page'

end
