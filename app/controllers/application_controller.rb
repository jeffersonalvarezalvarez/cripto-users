class ApplicationController < ActionController::Base
	before_action :configure_permitted_parameters, if: :devise_controller?
  	protect_from_forgery with: :exception

	# Método para permitir atributos en el registro
	def configure_permitted_parameters
		sign_up = [:email, :password, :private_key, :public_key]
		devise_parameter_sanitizer.permit :sign_up, keys: sign_up
	end

end
