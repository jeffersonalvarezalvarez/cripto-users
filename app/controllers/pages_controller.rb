class PagesController < ApplicationController
	before_action :authenticate_user!

	require 'net/http'

	# Aquí simplemente estará el formulario de carga
	def form
		@user = current_user
	end

	# Método para obtener los archivos que ha subido el usuario en la plataforma (Consulta)
	def search

		@user = current_user

		# Método para hacer un llamado a la API y buscar documentos de interés
		begin
			@documents = Net::HTTP.get(URI.parse("http://ejemplo_api.com/consulta/?pub_key="+@user.public_key))
		rescue => ex
		end

	end

	# Método para enviar archivo, llave pública y hash del archivo a la plataforma (Envío)
	def parameters

		file = params[:doc]
		public_key = params[:pub_key]
		hash = Digest::MD5.hexdigest(file)

		# Método para hacer un llamado a la API y enviar los objetos de interés
		begin
			Net::HTTP.get(URI.parse("http://ejemplo_api.com/upload/?pub_key="+public_key+"&file="+file+"&hash="+hash))
		rescue => ex
		end

	end
		
end
