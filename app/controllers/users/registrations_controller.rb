class Users::RegistrationsController < Devise::RegistrationsController

  require 'sshkey'

  def create

    # Se genera un par de llaves
    k = SSHKey.generate

    # Método por defecto para crear un usuario
    build_resource(sign_up_params)

    # Dentro de la base de datos creamos la llaves y las guardamos de forma encriptada
    resource.public_key = k.public_key
    resource.private_key = k.encrypted_private_key
    resource.save

    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
    
  end

end