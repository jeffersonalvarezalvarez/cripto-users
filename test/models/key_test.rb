require 'test/unit'
require 'sshkey'

# Test para identificar si general el par de llaves y son íntegras
class KeyTest < Test::Unit::TestCase
  def test_generation_keypair

  	# Se crea un par de llaves (Pública y privada)
  	k = SSHKey.generate

  	# Obtenemos los bits dentro de la pareja de llaves generada
  	bits = k.bits
  	
  	# Se hace la prueba para ver si se creo la llave de forma correcta
	assert bits == 2048

  end
end
