require 'test/unit'
require 'tempfile'
require 'net/http'
require 'digest/md5'

# Test para identificar si genera el Hash del archivo correctamente
class HashTest < Test::Unit::TestCase
  def test_hash_file

  	file = Tempfile.new('temp_file')

  	begin
  		Net::HTTP.start("www.planetebook.com") { |http|
    		resp = http.get("/ebooks/Moby-Dick.pdf")
    		file.write(resp.body)
  		}
  		hash = Digest::MD5.hexdigest(File.read(file))
	ensure
  		file.close
  		file.unlink
	end

	assert hash

  end
end
