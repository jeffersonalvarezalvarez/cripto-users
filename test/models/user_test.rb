require 'test/unit'
require 'sshkey'

# Test para identificar si general el par de llaves de forma correcta
class UserTest < Test::Unit::TestCase
  def test_generation_keypair

  	# Se crea un par de llaves (Pública y privada)
  	k = SSHKey.generate

  	# Se hace la prueba para ser si en verdad creó las llaves
	assert (k.public_key.present? && k.private_key.present?)
	 	
  end
end
